object Form1: TForm1
  Left = 0
  Top = 0
  Caption = 'Form1'
  ClientHeight = 415
  ClientWidth = 823
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 823
    Height = 57
    Align = alTop
    TabOrder = 0
    object Label1: TLabel
      Left = 8
      Top = 8
      Width = 70
      Height = 13
      Align = alCustom
      Caption = 'Developer Key'
    end
    object Label2: TLabel
      Left = 215
      Top = 8
      Width = 82
      Height = 13
      Align = alCustom
      Caption = 'Application Name'
    end
    object Label3: TLabel
      Left = 327
      Top = 8
      Width = 113
      Height = 13
      Align = alCustom
      Caption = 'Application Key (Token)'
    end
    object Button1: TButton
      Left = 719
      Top = 21
      Width = 75
      Height = 25
      Caption = 'Exec'
      TabOrder = 0
      OnClick = Button1Click
    end
    object edDeveloperKey: TEdit
      Left = 8
      Top = 23
      Width = 201
      Height = 21
      TabOrder = 1
      Text = 'a932ba8fc44e21acf445e096ad9253be'
    end
    object edApplicationName: TEdit
      Left = 215
      Top = 23
      Width = 106
      Height = 21
      TabOrder = 2
      Text = 'SysAid2Trello'
    end
    object edToken: TEdit
      Left = 327
      Top = 23
      Width = 386
      Height = 21
      TabOrder = 3
      Text = '47619a51aee46b92cd044b9e6583e804f7aaef8b1acbbe5d692a76c3a7feec23'
    end
  end
  object Memo: TMemo
    Left = 0
    Top = 57
    Width = 823
    Height = 358
    Align = alClient
    TabOrder = 1
  end
end
