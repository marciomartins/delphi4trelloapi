unit Main;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.ExtCtrls, System.Win.ComObj, Winapi.ActiveX;

type
  TForm1 = class(TForm)
    Panel1: TPanel;
    Button1: TButton;
    edDeveloperKey: TEdit;
    Label1: TLabel;
    edApplicationName: TEdit;
    Label2: TLabel;
    Label3: TLabel;
    edToken: TEdit;
    Memo: TMemo;
    procedure Button1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

uses HTTPApp, IdHTTP, IdSSLOpenSSL, IdGlobal;

{$R *.dfm}

const
  ServerAddress = 'https://api.trello.com/';
  APIVersion = '1';

procedure TForm1.Button1Click(Sender: TObject);
var
  CoResult: Integer;
  HTTP: TIdHTTP;
  Query: String;
  Buffer: String;
  lIOHandler: TIdSSLIOHandlerSocketOpenSSL;
  lParams :TStringList;
begin
  try
    //CoResult := CoInitializeEx(nil, COINIT_MULTITHREADED);
    CoResult := 0;
    if not((CoResult = S_OK) or (CoResult = S_FALSE)) then
    begin
      ShowMessage('Failed to initialize COM library.');
      Exit;
    end;
    HTTP := TIdHTTP.Create;
    lIOHandler := TIdSSLIOHandlerSocketOpenSSL.Create(nil);
    lParams := TStringList.Create;
    try
      HTTP.IOHandler := lIOHandler;
      HTTP.HandleRedirects := True;
//      Query := ServerAddress + '/boards/53e51fd42a0529b83092718e' +
//        '?lists=open&list_fields=name&fields=name,desc&key='+Key ;
//      Query := ServerAddress + ApiVersion+'/members/me/boards?key='+
//      edDeveloperKey.Text+'&token='+edToken.Text ;
      // HTTP GET request
      //Buffer := HTTP.Get(Query);

      //POST
      Query := 'https://api.trello.com/1/cards?key=a932ba8fc44e21acf445e096ad9253be&token=3050dab58655cab5f37eaebcb22bdce29996e72235ca422e19ccc34ed1c6ef13&name=My+new+card+name&desc=My+new+card+description&idList=53e5239227735a9c092f6188';
      Buffer := HTTP.Post(Query, lParams);
      Memo.Text := Buffer;
    finally
      lIOHandler.Free;
      HTTP.Destroy;
      lParams.Free;
    end;
  except
    on E: Exception do
      ShowMessage(E.ClassName + ': ' + E.Message);
  end;
{
uses
  SysUtils,
  HTTPApp,
  IdHTTP,
  XMLDoc,
  XMLIntf,
  ActiveX,
  IdSSLOpenSSL;

const
  ServerAddress = 'https://api.trello.com/1/';
  Key = 'a932ba8fc44e21acf445e096ad9253be';

var
  CoResult: Integer;
  HTTP: TIdHTTP;
  Query: String;
  Buffer: String;
  Doc: IXMLDocument;
  Node: IXMLNode;
  lIOHandler: TIdSSLIOHandlerSocketOpenSSL;

begin
  try
    CoResult := CoInitializeEx(nil, COINIT_MULTITHREADED);

    if not((CoResult = S_OK) or (CoResult = S_FALSE)) then
    begin
      Writeln('Failed to initialize COM library.');
      Exit;
    end;
    HTTP := TIdHTTP.Create;
    lIOHandler := TIdSSLIOHandlerSocketOpenSSL.Create(nil);
    HTTP.IOHandler := lIOHandler;
    HTTP.HandleRedirects := True;
    Query := ServerAddress + '/boards/53e51fd42a0529b83092718e' +
      '?lists=open&list_fields=name&fields=name,desc&key='+Key ;

    // HTTP GET request
//    Buffer := HTTP.Get(Query);
    Buffer := HTTP.Get(Query);

    HTTP.Destroy;
    lIOHandler.Free;
  except
    on E: Exception do
      Writeln(E.ClassName, ': ', E.Message);
  end;}
end;

end.
